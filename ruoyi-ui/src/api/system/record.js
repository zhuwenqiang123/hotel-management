import request from '@/utils/request'

// 查询入住记录列表
export function listRecord(query) {
  return request({
    url: '/system/record/list',
    method: 'get',
    params: query
  })
}

// 查询入住记录详细
export function getRecord(id) {
  return request({
    url: '/system/record/' + id,
    method: 'get'
  })
}
// 我的订单
export function myRcord(query) {
  return request({
    url: '/system/record/myRcord',
    method: 'get',
    params: query
  })
}


// 新增入住记录
export function addRecord(data) {
  return request({
    url: '/system/record',
    method: 'post',
    data: data
  })
}

// 修改入住记录
export function updateRecord(data) {
  return request({
    url: '/system/record',
    method: 'put',
    data: data
  })
}
// 评分
export function goscore(data) {
  return request({
    url: '/system/score',
    method: 'post',
    data: data
  })
}
// 查询评价
export function getscorelist(query) {
  return request({
    url: '/system/score/list',
    method: 'get',
    params: query
  })
}
export function getscoreall(query) {
  return request({
    url: '/system/score/all',
    method: 'get',
    params: query
  })
}
// 删除入住记录
export function delRecord(id) {
  return request({
    url: '/system/record/' + id,
    method: 'delete'
  })
}
