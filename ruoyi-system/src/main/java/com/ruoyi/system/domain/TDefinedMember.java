package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员信息对象 t_defined_member
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public class TDefinedMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 会员号 */
    @Excel(name = "会员号")
    private String menberNO;

    /** 会员姓名 */
    @Excel(name = "会员姓名")
    private String memberName;

    /** 会员性别 */
    @Excel(name = "会员性别")
    private String memberSex;

    /** 会员联系方式 */
    @Excel(name = "会员联系方式")
    private String memberMobile;

    /** 会员身份证号 */
    @Excel(name = "会员身份证号")
    private String identityCard;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMenberNO(String menberNO) 
    {
        this.menberNO = menberNO;
    }

    public String getMenberNO() 
    {
        return menberNO;
    }
    public void setMemberName(String memberName) 
    {
        this.memberName = memberName;
    }

    public String getMemberName() 
    {
        return memberName;
    }
    public void setMemberSex(String memberSex) 
    {
        this.memberSex = memberSex;
    }

    public String getMemberSex() 
    {
        return memberSex;
    }
    public void setMemberMobile(String memberMobile) 
    {
        this.memberMobile = memberMobile;
    }

    public String getMemberMobile() 
    {
        return memberMobile;
    }
    public void setIdentityCard(String identityCard) 
    {
        this.identityCard = identityCard;
    }

    public String getIdentityCard() 
    {
        return identityCard;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("menberNO", getMenberNO())
            .append("memberName", getMemberName())
            .append("memberSex", getMemberSex())
            .append("memberMobile", getMemberMobile())
            .append("identityCard", getIdentityCard())
            .append("createTime", getCreateTime())
            .toString();
    }
}
