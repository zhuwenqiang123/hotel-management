package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 入住记录对象 t_defined_check_record
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public class TDefinedCheckRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private String id;

    /**  */
    @Excel(name = "")
    private String checkInNo;

    /**  */
    @Excel(name = "")
    private String checkInTime;

    /**  */
    @Excel(name = "")
    private String checkInRoom;

    /**  */
    @Excel(name = "")
    private String checkInUserName;

    /**  */
    @Excel(name = "")
    private String checkOutTime;

    /**  */
    @Excel(name = "")
    private BigDecimal price;

    /**  */
    @Excel(name = "")
    private String createUser;

    private String phone;

    private String checkInUserId;

    private String status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCheckInNo(String checkInNo) 
    {
        this.checkInNo = checkInNo;
    }

    public String getCheckInNo() 
    {
        return checkInNo;
    }
    public void setCheckInTime(String checkInTime) 
    {
        this.checkInTime = checkInTime;
    }

    public String getCheckInTime() 
    {
        return checkInTime;
    }
    public void setCheckInRoom(String checkInRoom) 
    {
        this.checkInRoom = checkInRoom;
    }

    public String getCheckInRoom() 
    {
        return checkInRoom;
    }
    public void setCheckInUserName(String checkInUserName) 
    {
        this.checkInUserName = checkInUserName;
    }

    public String getCheckInUserName() 
    {
        return checkInUserName;
    }
    public void setCheckOutTime(String checkOutTime) 
    {
        this.checkOutTime = checkOutTime;
    }

    public String getCheckOutTime() 
    {
        return checkOutTime;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setCreateUser(String createUser) 
    {
        this.createUser = createUser;
    }

    public String getCreateUser() 
    {
        return createUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCheckInUserId() {
        return checkInUserId;
    }

    public void setCheckInUserId(String checkInUserId) {
        this.checkInUserId = checkInUserId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("checkInNo", getCheckInNo())
            .append("checkInTime", getCheckInTime())
            .append("checkInRoom", getCheckInRoom())
            .append("checkInUserName", getCheckInUserName())
            .append("checkOutTime", getCheckOutTime())
            .append("price", getPrice())
            .append("createUser", getCreateUser())
            .toString();
    }
}
