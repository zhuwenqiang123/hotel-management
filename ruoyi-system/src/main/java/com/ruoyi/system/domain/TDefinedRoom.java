package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客房管理对象 t_defined_room
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public class TDefinedRoom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 房间号 */
    @Excel(name = "房间号")
    private String roomId;

    /** 房间类型 */
    @Excel(name = "房间类型")
    private String roomType;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 所在楼层 */
    @Excel(name = "所在楼层")
    private String floor;

    /** 面积 */
    @Excel(name = "面积")
    private String area;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRoomId(String roomId) 
    {
        this.roomId = roomId;
    }

    public String getRoomId() 
    {
        return roomId;
    }
    public void setRoomType(String roomType) 
    {
        this.roomType = roomType;
    }

    public String getRoomType() 
    {
        return roomType;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setFloor(String floor) 
    {
        this.floor = floor;
    }

    public String getFloor() 
    {
        return floor;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("roomId", getRoomId())
            .append("roomType", getRoomType())
            .append("status", getStatus())
            .append("price", getPrice())
            .append("floor", getFloor())
            .append("area", getArea())
            .toString();
    }
}
