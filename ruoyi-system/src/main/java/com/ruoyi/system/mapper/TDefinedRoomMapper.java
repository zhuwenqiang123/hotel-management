package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TDefinedRoom;

/**
 * 客房管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface TDefinedRoomMapper 
{
    /**
     * 查询客房管理
     * 
     * @param id 客房管理主键
     * @return 客房管理
     */
    public TDefinedRoom selectTDefinedRoomById(String id);

    /**
     * 查询客房管理列表
     * 
     * @param tDefinedRoom 客房管理
     * @return 客房管理集合
     */
    public List<TDefinedRoom> selectTDefinedRoomList(TDefinedRoom tDefinedRoom);

    /**
     * 新增客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    public int insertTDefinedRoom(TDefinedRoom tDefinedRoom);

    /**
     * 修改客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    public int updateTDefinedRoom(TDefinedRoom tDefinedRoom);

    /**
     * 删除客房管理
     * 
     * @param id 客房管理主键
     * @return 结果
     */
    public int deleteTDefinedRoomById(String id);

    /**
     * 批量删除客房管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTDefinedRoomByIds(String[] ids);
}
