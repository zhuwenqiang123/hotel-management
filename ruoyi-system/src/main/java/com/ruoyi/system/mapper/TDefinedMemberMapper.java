package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TDefinedMember;

/**
 * 会员信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface TDefinedMemberMapper 
{
    /**
     * 查询会员信息
     * 
     * @param id 会员信息主键
     * @return 会员信息
     */
    public TDefinedMember selectTDefinedMemberById(String id);

    /**
     * 查询会员信息列表
     * 
     * @param tDefinedMember 会员信息
     * @return 会员信息集合
     */
    public List<TDefinedMember> selectTDefinedMemberList(TDefinedMember tDefinedMember);

    /**
     * 新增会员信息
     * 
     * @param tDefinedMember 会员信息
     * @return 结果
     */
    public int insertTDefinedMember(TDefinedMember tDefinedMember);

    /**
     * 修改会员信息
     * 
     * @param tDefinedMember 会员信息
     * @return 结果
     */
    public int updateTDefinedMember(TDefinedMember tDefinedMember);

    /**
     * 删除会员信息
     * 
     * @param id 会员信息主键
     * @return 结果
     */
    public int deleteTDefinedMemberById(String id);

    /**
     * 批量删除会员信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTDefinedMemberByIds(String[] ids);
}
