package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TDefinedScore;

/**
 * 酒店评分Service接口
 * 
 * @author ruoyi
 * @date 2024-03-12
 */
public interface ITDefinedScoreService 
{
    /**
     * 查询酒店评分
     * 
     * @param id 酒店评分主键
     * @return 酒店评分
     */
    public TDefinedScore selectTDefinedScoreById(String id);

    /**
     * 查询酒店评分列表
     * 
     * @param tDefinedScore 酒店评分
     * @return 酒店评分集合
     */
    public List<TDefinedScore> selectTDefinedScoreList(TDefinedScore tDefinedScore);

    /**
     * 新增酒店评分
     * 
     * @param tDefinedScore 酒店评分
     * @return 结果
     */
    public int insertTDefinedScore(TDefinedScore tDefinedScore);

    /**
     * 修改酒店评分
     * 
     * @param tDefinedScore 酒店评分
     * @return 结果
     */
    public int updateTDefinedScore(TDefinedScore tDefinedScore);

    /**
     * 批量删除酒店评分
     * 
     * @param ids 需要删除的酒店评分主键集合
     * @return 结果
     */
    public int deleteTDefinedScoreByIds(String[] ids);

    /**
     * 删除酒店评分信息
     * 
     * @param id 酒店评分主键
     * @return 结果
     */
    public int deleteTDefinedScoreById(String id);
}
