package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.system.domain.TDefinedCheckRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TDefinedRoomMapper;
import com.ruoyi.system.domain.TDefinedRoom;
import com.ruoyi.system.service.ITDefinedRoomService;

/**
 * 客房管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class TDefinedRoomServiceImpl implements ITDefinedRoomService 
{
    @Autowired
    private TDefinedRoomMapper tDefinedRoomMapper;

    /**
     * 查询客房管理
     * 
     * @param id 客房管理主键
     * @return 客房管理
     */
    @Override
    public TDefinedRoom selectTDefinedRoomById(String id)
    {
        return tDefinedRoomMapper.selectTDefinedRoomById(id);
    }

    /**
     * 查询客房管理列表
     * 
     * @param tDefinedRoom 客房管理
     * @return 客房管理
     */
    @Override
    public List<TDefinedRoom> selectTDefinedRoomList(TDefinedRoom tDefinedRoom)
    {
        return tDefinedRoomMapper.selectTDefinedRoomList(tDefinedRoom);
    }

    /**
     * 新增客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    @Override
    public int insertTDefinedRoom(TDefinedRoom tDefinedRoom)
    {
        tDefinedRoom.setId(UUID.randomUUID().toString());
        return tDefinedRoomMapper.insertTDefinedRoom(tDefinedRoom);
    }

    /**
     * 修改客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    @Override
    public int updateTDefinedRoom(TDefinedRoom tDefinedRoom)
    {
        return tDefinedRoomMapper.updateTDefinedRoom(tDefinedRoom);
    }

    /**
     * 批量删除客房管理
     * 
     * @param ids 需要删除的客房管理主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedRoomByIds(String[] ids)
    {
        return tDefinedRoomMapper.deleteTDefinedRoomByIds(ids);
    }

    /**
     * 删除客房管理信息
     * 
     * @param id 客房管理主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedRoomById(String id)
    {
        return tDefinedRoomMapper.deleteTDefinedRoomById(id);
    }
}
