package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TDefinedCheckRecord;

/**
 * 入住记录Service接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface ITDefinedCheckRecordService 
{
    /**
     * 查询入住记录
     * 
     * @param id 入住记录主键
     * @return 入住记录
     */
    public TDefinedCheckRecord selectTDefinedCheckRecordById(String id);

    /**
     * 查询入住记录列表
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 入住记录集合
     */
    public List<TDefinedCheckRecord> selectTDefinedCheckRecordList(TDefinedCheckRecord tDefinedCheckRecord);

    /**
     * 新增入住记录
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 结果
     */
    public int insertTDefinedCheckRecord(TDefinedCheckRecord tDefinedCheckRecord);

    /**
     * 修改入住记录
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 结果
     */
    public int updateTDefinedCheckRecord(TDefinedCheckRecord tDefinedCheckRecord);

    /**
     * 批量删除入住记录
     * 
     * @param ids 需要删除的入住记录主键集合
     * @return 结果
     */
    public int deleteTDefinedCheckRecordByIds(String[] ids);

    /**
     * 删除入住记录信息
     * 
     * @param id 入住记录主键
     * @return 结果
     */
    public int deleteTDefinedCheckRecordById(String id);

    public List<TDefinedCheckRecord> getMyRecord(String userId);
}
