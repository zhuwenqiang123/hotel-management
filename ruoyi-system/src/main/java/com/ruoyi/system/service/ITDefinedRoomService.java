package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TDefinedRoom;

/**
 * 客房管理Service接口
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
public interface ITDefinedRoomService 
{
    /**
     * 查询客房管理
     * 
     * @param id 客房管理主键
     * @return 客房管理
     */
    public TDefinedRoom selectTDefinedRoomById(String id);

    /**
     * 查询客房管理列表
     * 
     * @param tDefinedRoom 客房管理
     * @return 客房管理集合
     */
    public List<TDefinedRoom> selectTDefinedRoomList(TDefinedRoom tDefinedRoom);

    /**
     * 新增客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    public int insertTDefinedRoom(TDefinedRoom tDefinedRoom);

    /**
     * 修改客房管理
     * 
     * @param tDefinedRoom 客房管理
     * @return 结果
     */
    public int updateTDefinedRoom(TDefinedRoom tDefinedRoom);

    /**
     * 批量删除客房管理
     * 
     * @param ids 需要删除的客房管理主键集合
     * @return 结果
     */
    public int deleteTDefinedRoomByIds(String[] ids);

    /**
     * 删除客房管理信息
     * 
     * @param id 客房管理主键
     * @return 结果
     */
    public int deleteTDefinedRoomById(String id);
}
