package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TDefinedMemberMapper;
import com.ruoyi.system.domain.TDefinedMember;
import com.ruoyi.system.service.ITDefinedMemberService;

/**
 * 会员信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class TDefinedMemberServiceImpl implements ITDefinedMemberService 
{
    @Autowired
    private TDefinedMemberMapper tDefinedMemberMapper;

    /**
     * 查询会员信息
     * 
     * @param id 会员信息主键
     * @return 会员信息
     */
    @Override
    public TDefinedMember selectTDefinedMemberById(String id)
    {
        return tDefinedMemberMapper.selectTDefinedMemberById(id);
    }

    /**
     * 查询会员信息列表
     * 
     * @param tDefinedMember 会员信息
     * @return 会员信息
     */
    @Override
    public List<TDefinedMember> selectTDefinedMemberList(TDefinedMember tDefinedMember)
    {
        return tDefinedMemberMapper.selectTDefinedMemberList(tDefinedMember);
    }

    /**
     * 新增会员信息
     * 
     * @param tDefinedMember 会员信息
     * @return 结果
     */
    @Override
    public int insertTDefinedMember(TDefinedMember tDefinedMember)
    {
        tDefinedMember.setId(UUID.randomUUID().toString());
        tDefinedMember.setCreateTime(DateUtils.getNowDate());
        return tDefinedMemberMapper.insertTDefinedMember(tDefinedMember);
    }

    /**
     * 修改会员信息
     * 
     * @param tDefinedMember 会员信息
     * @return 结果
     */
    @Override
    public int updateTDefinedMember(TDefinedMember tDefinedMember)
    {
        return tDefinedMemberMapper.updateTDefinedMember(tDefinedMember);
    }

    /**
     * 批量删除会员信息
     * 
     * @param ids 需要删除的会员信息主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedMemberByIds(String[] ids)
    {
        return tDefinedMemberMapper.deleteTDefinedMemberByIds(ids);
    }

    /**
     * 删除会员信息信息
     * 
     * @param id 会员信息主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedMemberById(String id)
    {
        return tDefinedMemberMapper.deleteTDefinedMemberById(id);
    }
}
