package com.ruoyi.system.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.ruoyi.system.domain.TDefinedRoom;
import com.ruoyi.system.mapper.TDefinedRoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TDefinedCheckRecordMapper;
import com.ruoyi.system.domain.TDefinedCheckRecord;
import com.ruoyi.system.service.ITDefinedCheckRecordService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 入住记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class TDefinedCheckRecordServiceImpl implements ITDefinedCheckRecordService 
{
    @Autowired
    private TDefinedCheckRecordMapper tDefinedCheckRecordMapper;

    @Autowired
    private TDefinedRoomMapper tDefinedRoomMapper;

    /**
     * 查询入住记录
     * 
     * @param id 入住记录主键
     * @return 入住记录
     */
    @Override
    public TDefinedCheckRecord selectTDefinedCheckRecordById(String id)
    {
        return tDefinedCheckRecordMapper.selectTDefinedCheckRecordById(id);
    }

    /**
     * 查询入住记录列表
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 入住记录
     */
    @Override
    public List<TDefinedCheckRecord> selectTDefinedCheckRecordList(TDefinedCheckRecord tDefinedCheckRecord)
    {
        return tDefinedCheckRecordMapper.selectTDefinedCheckRecordList(tDefinedCheckRecord);
    }

    /**
     * 新增入住记录
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 结果
     */
    @Override
    public int insertTDefinedCheckRecord(TDefinedCheckRecord tDefinedCheckRecord)
    {
        tDefinedCheckRecord.setId(UUID.randomUUID().toString());
        // 修改房间状态
        TDefinedRoom tDefinedRoom = new TDefinedRoom();
        tDefinedRoom.setRoomId(tDefinedCheckRecord.getCheckInRoom());
        tDefinedRoom.setStatus("2");
        tDefinedRoomMapper.updateTDefinedRoom(tDefinedRoom);
        int count = tDefinedCheckRecordMapper.selectCount();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
        String format = simpleDateFormat.format(new Date());
        tDefinedCheckRecord.setCheckInNo(format+"00"+count);
        return tDefinedCheckRecordMapper.insertTDefinedCheckRecord(tDefinedCheckRecord);
    }

    /**
     * 修改入住记录
     * 
     * @param tDefinedCheckRecord 入住记录
     * @return 结果
     */
    @Override
    public int updateTDefinedCheckRecord(TDefinedCheckRecord tDefinedCheckRecord)
    {
        TDefinedRoom tDefinedRoom = new TDefinedRoom();
        tDefinedRoom.setRoomId(tDefinedCheckRecord.getCheckInRoom());
        tDefinedRoom.setStatus(tDefinedCheckRecord.getStatus());
        tDefinedRoomMapper.updateTDefinedRoom(tDefinedRoom);
        return tDefinedCheckRecordMapper.updateTDefinedCheckRecord(tDefinedCheckRecord);
    }

    /**
     * 批量删除入住记录
     * 
     * @param ids 需要删除的入住记录主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedCheckRecordByIds(String[] ids)
    {
        return tDefinedCheckRecordMapper.deleteTDefinedCheckRecordByIds(ids);
    }

    /**
     * 删除入住记录信息
     * 
     * @param id 入住记录主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedCheckRecordById(String id)
    {
        return tDefinedCheckRecordMapper.deleteTDefinedCheckRecordById(id);
    }

    @Override
    public List<TDefinedCheckRecord> getMyRecord(String userId) {
        return tDefinedCheckRecordMapper.getMyRecord(userId);
    }
}
