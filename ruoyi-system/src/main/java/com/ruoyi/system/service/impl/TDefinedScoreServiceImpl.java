package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TDefinedScoreMapper;
import com.ruoyi.system.domain.TDefinedScore;
import com.ruoyi.system.service.ITDefinedScoreService;

/**
 * 酒店评分Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-12
 */
@Service
public class TDefinedScoreServiceImpl implements ITDefinedScoreService 
{
    @Autowired
    private TDefinedScoreMapper tDefinedScoreMapper;

    /**
     * 查询酒店评分
     * 
     * @param id 酒店评分主键
     * @return 酒店评分
     */
    @Override
    public TDefinedScore selectTDefinedScoreById(String id)
    {
        return tDefinedScoreMapper.selectTDefinedScoreById(id);
    }

    /**
     * 查询酒店评分列表
     * 
     * @param tDefinedScore 酒店评分
     * @return 酒店评分
     */
    @Override
    public List<TDefinedScore> selectTDefinedScoreList(TDefinedScore tDefinedScore)
    {
        return tDefinedScoreMapper.selectTDefinedScoreList(tDefinedScore);
    }

    /**
     * 新增酒店评分
     * 
     * @param tDefinedScore 酒店评分
     * @return 结果
     */
    @Override
    public int insertTDefinedScore(TDefinedScore tDefinedScore)
    {
        tDefinedScore.setId(UUID.randomUUID().toString());
        return tDefinedScoreMapper.insertTDefinedScore(tDefinedScore);
    }

    /**
     * 修改酒店评分
     * 
     * @param tDefinedScore 酒店评分
     * @return 结果
     */
    @Override
    public int updateTDefinedScore(TDefinedScore tDefinedScore)
    {
        return tDefinedScoreMapper.updateTDefinedScore(tDefinedScore);
    }

    /**
     * 批量删除酒店评分
     * 
     * @param ids 需要删除的酒店评分主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedScoreByIds(String[] ids)
    {
        return tDefinedScoreMapper.deleteTDefinedScoreByIds(ids);
    }

    /**
     * 删除酒店评分信息
     * 
     * @param id 酒店评分主键
     * @return 结果
     */
    @Override
    public int deleteTDefinedScoreById(String id)
    {
        return tDefinedScoreMapper.deleteTDefinedScoreById(id);
    }
}
