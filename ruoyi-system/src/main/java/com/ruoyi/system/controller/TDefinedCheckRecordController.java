package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TDefinedCheckRecord;
import com.ruoyi.system.service.ITDefinedCheckRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 入住记录Controller
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/system/record")
public class TDefinedCheckRecordController extends BaseController
{
    @Autowired
    private ITDefinedCheckRecordService tDefinedCheckRecordService;

    /**
     * 查询入住记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(TDefinedCheckRecord tDefinedCheckRecord)
    {
        startPage();
        List<TDefinedCheckRecord> list = tDefinedCheckRecordService.selectTDefinedCheckRecordList(tDefinedCheckRecord);
        return getDataTable(list);
    }

    /**
     * 导出入住记录列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "入住记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TDefinedCheckRecord tDefinedCheckRecord)
    {
        List<TDefinedCheckRecord> list = tDefinedCheckRecordService.selectTDefinedCheckRecordList(tDefinedCheckRecord);
        ExcelUtil<TDefinedCheckRecord> util = new ExcelUtil<TDefinedCheckRecord>(TDefinedCheckRecord.class);
        util.exportExcel(response, list, "入住记录数据");
    }

    /**
     * 获取入住记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(tDefinedCheckRecordService.selectTDefinedCheckRecordById(id));
    }

    /**
     * 新增入住记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:add')")
    @Log(title = "入住记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TDefinedCheckRecord tDefinedCheckRecord)
    {
        return toAjax(tDefinedCheckRecordService.insertTDefinedCheckRecord(tDefinedCheckRecord));
    }

    /**
     * 修改入住记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:edit')")
    @Log(title = "入住记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TDefinedCheckRecord tDefinedCheckRecord)
    {
        return toAjax(tDefinedCheckRecordService.updateTDefinedCheckRecord(tDefinedCheckRecord));
    }

    /**
     * 删除入住记录
     */
    @PreAuthorize("@ss.hasPermi('system:record:remove')")
    @Log(title = "入住记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tDefinedCheckRecordService.deleteTDefinedCheckRecordByIds(ids));
    }

    /**
     * 我的订单
     */
    @PreAuthorize("@ss.hasPermi('system:record:myRcord')")
    @Log(title = "我的入住记录")
    @GetMapping(value = "/myRcord")
    public TableDataInfo myRcord(String userId)
    {
        startPage();
        List<TDefinedCheckRecord> list = tDefinedCheckRecordService.getMyRecord(userId);
        return getDataTable(list);
    }
}
