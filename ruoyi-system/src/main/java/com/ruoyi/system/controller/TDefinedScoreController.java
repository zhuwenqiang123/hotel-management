package com.ruoyi.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TDefinedScore;
import com.ruoyi.system.service.ITDefinedScoreService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 酒店评分Controller
 * 
 * @author ruoyi
 * @date 2024-03-12
 */
@RestController
@RequestMapping("/system/score")
public class TDefinedScoreController extends BaseController
{
    @Autowired
    private ITDefinedScoreService tDefinedScoreService;

    /**
     * 查询酒店评分列表
     */
    @GetMapping("/list")
    public TableDataInfo list(TDefinedScore tDefinedScore)
    {
        startPage();
        List<TDefinedScore> list = tDefinedScoreService.selectTDefinedScoreList(tDefinedScore);
        return getDataTable(list);
    }

    @GetMapping("/all")
    public AjaxResult all(TDefinedScore tDefinedScore)
    {
        List<TDefinedScore> list = tDefinedScoreService.selectTDefinedScoreList(tDefinedScore);
        return success(list);
    }

    /**
     * 导出酒店评分列表
     */
    @PreAuthorize("@ss.hasPermi('system:score:export')")
    @Log(title = "酒店评分", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TDefinedScore tDefinedScore)
    {
        List<TDefinedScore> list = tDefinedScoreService.selectTDefinedScoreList(tDefinedScore);
        ExcelUtil<TDefinedScore> util = new ExcelUtil<TDefinedScore>(TDefinedScore.class);
        util.exportExcel(response, list, "酒店评分数据");
    }

    /**
     * 获取酒店评分详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:score:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(tDefinedScoreService.selectTDefinedScoreById(id));
    }

    /**
     * 新增酒店评分
     */
    @PreAuthorize("@ss.hasPermi('system:score:add')")
    @Log(title = "酒店评分", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TDefinedScore tDefinedScore)
    {
        return toAjax(tDefinedScoreService.insertTDefinedScore(tDefinedScore));
    }

    /**
     * 修改酒店评分
     */
    @PreAuthorize("@ss.hasPermi('system:score:edit')")
    @Log(title = "酒店评分", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TDefinedScore tDefinedScore)
    {
        return toAjax(tDefinedScoreService.updateTDefinedScore(tDefinedScore));
    }

    /**
     * 删除酒店评分
     */
    @PreAuthorize("@ss.hasPermi('system:score:remove')")
    @Log(title = "酒店评分", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tDefinedScoreService.deleteTDefinedScoreByIds(ids));
    }
}
