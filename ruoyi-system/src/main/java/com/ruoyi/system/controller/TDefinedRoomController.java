package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TDefinedRoom;
import com.ruoyi.system.service.ITDefinedRoomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 客房管理Controller
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/system/room")
public class TDefinedRoomController extends BaseController
{
    @Autowired
    private ITDefinedRoomService tDefinedRoomService;

    /**
     * 查询客房管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:room:list')")
    @GetMapping("/list")
    public TableDataInfo list(TDefinedRoom tDefinedRoom)
    {
        startPage();
        List<TDefinedRoom> list = tDefinedRoomService.selectTDefinedRoomList(tDefinedRoom);
        return getDataTable(list);
    }

    /**
     * 导出客房管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:room:export')")
    @Log(title = "客房管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TDefinedRoom tDefinedRoom)
    {
        List<TDefinedRoom> list = tDefinedRoomService.selectTDefinedRoomList(tDefinedRoom);
        ExcelUtil<TDefinedRoom> util = new ExcelUtil<TDefinedRoom>(TDefinedRoom.class);
        util.exportExcel(response, list, "客房管理数据");
    }

    /**
     * 获取客房管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:room:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(tDefinedRoomService.selectTDefinedRoomById(id));
    }

    /**
     * 新增客房管理
     */
    @PreAuthorize("@ss.hasPermi('system:room:add')")
    @Log(title = "客房管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TDefinedRoom tDefinedRoom)
    {
        return toAjax(tDefinedRoomService.insertTDefinedRoom(tDefinedRoom));
    }

    /**
     * 修改客房管理
     */
    @PreAuthorize("@ss.hasPermi('system:room:edit')")
    @Log(title = "客房管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TDefinedRoom tDefinedRoom)
    {
        return toAjax(tDefinedRoomService.updateTDefinedRoom(tDefinedRoom));
    }

    /**
     * 删除客房管理
     */
    @PreAuthorize("@ss.hasPermi('system:room:remove')")
    @Log(title = "客房管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tDefinedRoomService.deleteTDefinedRoomByIds(ids));
    }
}
